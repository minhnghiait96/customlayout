import * as React from 'react'
import { Menu, Icon ,Layout} from 'antd';
const { SubMenu } = Menu
const { Sider } = Layout;
import { MenuModel } from '../../interfaces/menu'
interface Props {
  isToggle?: boolean,
  listSidebar: MenuModel[]
}
const defaultProps: Props = {
  isToggle: false,
  listSidebar: []
}
const Sidebar: React.FunctionComponent<Props> = (props) => {
  const categoryMenu = (cateMenu: MenuModel) => {
    if (cateMenu.children) {
      return (
        <SubMenu
          key={cateMenu.id}
          title={
            <span>
              {cateMenu.iconType && <Icon type={cateMenu.iconType} />}
              <span>{cateMenu.name}</span>
            </span>
          }
        >
          {
            createSubmenu(cateMenu.children)
          }
        </SubMenu>
      )
    } else {
      return (
        <Menu.Item key={cateMenu.id}>
          {cateMenu.iconType && <Icon type={cateMenu.iconType} />}
          <span>{cateMenu.name}</span>
        </Menu.Item>
      )
    }
  }
  const createSubmenu = (listSubmenu: MenuModel[]) => (
    listSubmenu.map((item) => categoryMenu(item))
  )
  return (
    <Sider width={'220'} className="sider" collapsed={props.isToggle}>
      <Menu
        mode="inline"
        theme="dark"
      >
        {
          props.listSidebar.map((menuItem) => (
            categoryMenu(menuItem)
          ))
        }
      </Menu>
      <style>{`
        .sider {
          height: 100vh;
        }

        @media (max-width: 576px) {
          .sider {
            height: 100%;
            width: 100% !important;
            flex: 0 0 100% !important;
            max-width: 100% !important;
            min-width: 100% !important;
          }
        }
      `}</style>
    </Sider>
  )
}
Sidebar.defaultProps = defaultProps;

export default Sidebar;