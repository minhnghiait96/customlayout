import * as React from 'react'
import { Dropdown,Icon} from 'antd'
interface Props {
    menu:any
}
const GridMenuFooter:React.FunctionComponent<Props> = (props:Props) =>{
return (
    <Dropdown className="dropDown1" overlay={props.menu} trigger={['click']}>
            
    <span className="ant-dropdown-link" >
   
    &nbsp;&nbsp;Grid Menu <Icon type="down" />
    </span>
  </Dropdown>
)
}
export default GridMenuFooter;