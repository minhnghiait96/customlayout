import * as React from 'react'
import { Dropdown,Icon} from 'antd'
interface Props {
    menu:any
}
const FooterMenu:React.FunctionComponent<Props> = (props:Props) =>{
return (
    <Dropdown className="dropDown1" overlay={props.menu} trigger={['click']}>
            
    <span className="ant-dropdown-link" >
    <Icon type="gift" theme="twoTone" />
    &nbsp;&nbsp;Foot Menu <Icon type="down" />
    </span>
  </Dropdown>
)
}
export default FooterMenu;