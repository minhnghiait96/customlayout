import * as React from 'react'
import Navbar from './Navbar/Navbar'
import Footer from './Footer/Footer'
import SubHeader from './SubHeader/SubHeader'
import '../styles.scss'
interface Props{
  title?:string;
  IsDisplay?:boolean;
  children?: React.ReactNode
}
  const Layout: React.FunctionComponent<Props> = (props:Props) => {
  return (
    <div className="layout" >
      <header>
        <Navbar></Navbar>
        <SubHeader title={props.title} IsDisplay={props.IsDisplay}></SubHeader>
      </header>
      <div className="main">
        <div className="mainContent">
          <article>  
          </article>
          <footer><Footer></Footer></footer>
        </div>
      </div>
    </div>
  )
}

export default Layout
