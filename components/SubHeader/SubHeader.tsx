import * as React from 'react'
import { Icon, Button } from 'antd'
import Router from 'next/router'
import './styles.scss'
interface Props {
    title?: string;
    IsDisplay?: boolean;
}
const SubHeader: React.FunctionComponent<Props> = (props: Props) => {
    const handler = () => {
        Router.push({
            pathname: '/hex-network'
        })
    }
    const hanldeNetWork = () => {
        Router.push({
            pathname: '/content-network'
        })
      
    }
    return (
        <div className="subHeader">
            <div className="contentSub">
                <div className="titleSubComponent"> Home <Icon type="right" /> <a>{props.title}</a></div>
                <div className="titleAndButton">
                    <div className="bigTitle">
                        {props.title}
                    </div>
                    <div className={props.IsDisplay ? 'buttonRouter' : 'buttonRouter hidden'}>
                        <Button type="default" onClick={handler} >
                            View Hex NetWork
        <Icon type="right" />
                        </Button>           
                    </div>     
                </div>
            </div>
            <div className={props.IsDisplay? 'hidden':''}>
            <hr/>
            <div className="detailInfo">
                <div className="list">List</div>
                <div className="networkDiagram" onClick={hanldeNetWork}>NetWork Diagram</div>
            </div>
            </div>
        </div>
    )
}
export default SubHeader;

