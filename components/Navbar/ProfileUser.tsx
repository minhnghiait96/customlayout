import * as React from 'react'
import { Dropdown, Icon, Menu } from 'antd'
const menu = (
  <Menu>
    <Menu.Item key="0">
      <a href="http://www.alipay.com/">1st menu item</a>
    </Menu.Item>
    <Menu.Item key="1">
      <a href="http://www.taobao.com/">2nd menu item</a>
    </Menu.Item>

    <Menu.Item key="3">3rd menu item</Menu.Item>
  </Menu>
)
const ProfileUser: React.FunctionComponent = () => {
  return (
    <Dropdown className="dropDown1"
      overlay={menu}
      trigger={['click']}>

      <span className="ant-dropdown-link" >

        <img className="circle" src="./../../static/img/avatar.png" width="34" /><Icon type="down" />
      </span>
    </Dropdown>
  )
}
export default ProfileUser;