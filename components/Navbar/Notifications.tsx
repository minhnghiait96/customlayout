import * as React from 'react'
import {Popover,Button,Icon} from 'antd'
const text = <span>Title</span>;
const content = (
  <div style={{width:'300px'}}>
    <p>Content</p>
    <p>Content</p>
  </div>
);
const Notifications:React.FunctionComponent = () =>{
  return(
      <span>
<Popover placement="bottom" title={text} content={content} trigger="click">
        <Button><Icon type="notification" theme="twoTone" /></Button>
      </Popover>
      </span>
  )
  
}
export default Notifications;
