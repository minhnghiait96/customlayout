import * as React from 'react'
import './styles.scss'

const Navbar: React.FunctionComponent = () => {
  const arrLogo = {
    imgLogo: 'https://adtimin.vn/wp-content/uploads/2017/09/Logo-1.jpg',
    title: 'Test'
  }
  const arrProfile = {
    imgProfile: 'http://thuthuat123.com/uploads/2018/01/27/Avatar-dep-nhat-83_112148.jpg',
    name: 'Nghia'
  }
  return (
    <nav>
      <div className="navbar">
        <div className="logo">
          <img src={arrLogo.imgLogo} height="50" width="50" />
          <span className="title">{arrLogo.title}</span>
        </div>
        <div className="profileUser">
          <span className="username">Hi,{arrProfile.name}</span>
          <img className="circle" src={arrProfile.imgProfile} width="34" />
        </div>
      </div>
    
    </nav>
  )
}
export default Navbar;